import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.util.*
import javax.swing.*
import javax.swing.JOptionPane.YES_OPTION


class Slider : JFrame,ActionListener {

    override fun actionPerformed(e: ActionEvent?) {

        if (e!!.source == ExitButton){

            var Temp : Any = JOptionPane.showConfirmDialog(this,"Do you want to exit ? " , "Warning",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE)

            if (Temp == YES_OPTION)
                dispose()

                                     }//if statement

        else if (e!!.source == Button1){

            if (TempArray[0][0] == 0){
                when (Counter) {
                    1 -> Button1.icon = Image1
                    2 -> Button1.icon = Image2
                    3 -> Button1.icon = Image3
                    4 -> Button1.icon = Image4
                    5 -> Button1.icon = Image5
                    6 -> Button1.icon = Image6
                    7 -> Button1.icon = Image7
                    8 -> Button1.icon = Image8
                                }//when statement

                if (Counter == 9) {
                    TempArray[0][0] = 0
                    Counter++
                                  }//if statement

                else {
                    TempArray[0][0] = Counter
                    Counter++
                     }//else statement

                                     }//if statement

            else if (Counter == 10){

                if (Button2.icon == EmptyImage){
                    Button2.icon = Button1.icon
                    Button1.icon = EmptyImage
                    TempArray[0][1] = TempArray[0][0]
                    TempArray[0][0] = 0
                                               }//if statement

                else if (Button4.icon == EmptyImage){
                    Button4.icon = Button1.icon
                    Button1.icon = EmptyImage
                    TempArray[1][0] = TempArray[0][0]
                    TempArray[0][0] = 0
                                                    }//else if statement

                                  }//else if statement

                                       }//else if statement

        else if (e!!.source == Button2){

            if (TempArray[0][1] == 0){
                when (Counter) {
                    1 -> Button2.icon = Image1
                    2 -> Button2.icon = Image2
                    3 -> Button2.icon = Image3
                    4 -> Button2.icon = Image4
                    5 -> Button2.icon = Image5
                    6 -> Button2.icon = Image6
                    7 -> Button2.icon = Image7
                    8 -> Button2.icon = Image8
                                }//when statement

                if (Counter == 9){
                    TempArray[0][1] = 0
                    Counter++
                                 }//if statement

                else{
                TempArray[0][1] = Counter
                Counter++
                    }//else statement

                                      }//if statement

            else if (Counter == 10){

                if (Button1.icon == EmptyImage){
                    Button1.icon = Button2.icon
                    Button2.icon = EmptyImage
                    TempArray[0][0] = TempArray[0][1]
                    TempArray[0][1] = 0
                                               }//if statement

                else if (Button5.icon == EmptyImage){
                    Button5.icon = Button2.icon
                    Button2.icon = EmptyImage
                    TempArray[1][1] = TempArray[0][1]
                    TempArray[0][1] = 0
                                                    }//else if statement

                else if (Button3.icon == EmptyImage){
                    Button3.icon = Button2.icon
                    Button2.icon = EmptyImage
                    TempArray[0][2] = TempArray[0][1]
                    TempArray[0][1] = 0
                                                    }//else if statement

                                   }//else if statement

                                        }//else if statement

        else if (e!!.source == Button3){

            if (TempArray[0][2] == 0){
                when (Counter) {
                    1 -> Button3.icon = Image1
                    2 -> Button3.icon = Image2
                    3 -> Button3.icon = Image3
                    4 -> Button3.icon = Image4
                    5 -> Button3.icon = Image5
                    6 -> Button3.icon = Image6
                    7 -> Button3.icon = Image7
                    8 -> Button3.icon = Image8
                               }//when

                if (Counter == 9){
                    TempArray[0][2] = 0
                    Counter++
                                 }//if statement
                else {
                    TempArray[0][2] = Counter
                    Counter++
                     }//else statement
                                       }//if statement

            else if (Counter == 10){

                if (Button2.icon == EmptyImage){
                    Button2.icon = Button3.icon
                    Button3.icon = EmptyImage
                    TempArray[0][1] = TempArray[0][2]
                    TempArray[0][2] = 0
                                                }//if statement

                else if (Button6.icon == EmptyImage){
                    Button6.icon = Button3.icon
                    Button3.icon = EmptyImage
                    TempArray[1][2] = TempArray[0][2]
                    TempArray[1][2] = 0
                                                    }//else if statement

                                    }//else if statement

                                        }//else if statement

        else if (e!!.source == Button4){

            if (TempArray[1][0] == 0){
                when (Counter) {
                    1 -> Button4.icon = Image1
                    2 -> Button4.icon = Image2
                    3 -> Button4.icon = Image3
                    4 -> Button4.icon = Image4
                    5 -> Button4.icon = Image5
                    6 -> Button4.icon = Image6
                    7 -> Button4.icon = Image7
                    8 -> Button4.icon = Image8
                               }//when

                if (Counter == 9){
                    TempArray[1][0] = 0
                    Counter++
                                 }//if statement
                else {
                    TempArray[1][0] = Counter
                    Counter++
                     }//else statement
                                      }//if statement

            else if (Counter == 10){

                if (Button1.icon == EmptyImage){
                    Button1.icon = Button4.icon
                    Button4.icon = EmptyImage
                    TempArray[0][0] = TempArray[1][0]
                    TempArray[1][0] = 0
                                               }//if statement

                else if (Button5.icon == EmptyImage){
                    Button5.icon = Button4.icon
                    Button4.icon = EmptyImage
                    TempArray[1][1] = TempArray[1][0]
                    TempArray[1][0] = 0
                                                    }//else if statement

                else if (Button7.icon == EmptyImage){
                    Button7.icon = Button4.icon
                    Button4.icon = EmptyImage
                    TempArray[2][0] = TempArray[1][0]
                    TempArray[1][0] = 0
                                                    }//else if statement

                                   }//else if statement

                                       }//else if statement

        else if (e!!.source == Button5){

            if (TempArray[1][1] == 0){
                when (Counter) {
                    1 -> Button5.icon = Image1
                    2 -> Button5.icon = Image2
                    3 -> Button5.icon = Image3
                    4 -> Button5.icon = Image4
                    5 -> Button5.icon = Image5
                    6 -> Button5.icon = Image6
                    7 -> Button5.icon = Image7
                    8 -> Button5.icon = Image8
                                }//when

                if (Counter == 9){
                    TempArray[1][1] = 0
                    Counter++
                                 }//if statement
                else {
                    TempArray[1][1] = Counter
                    Counter++
                     }//else statement
                                      }//if statement

            else if (Counter == 10){

                if (Button2.icon == EmptyImage){
                    Button2.icon = Button5.icon
                    Button5.icon = EmptyImage
                    TempArray[0][1] = TempArray[1][1]
                    TempArray[1][1] = 0
                                               }//if statement

                else if (Button4.icon == EmptyImage){
                    Button4.icon = Button5.icon
                    Button5.icon = EmptyImage
                    TempArray[1][0] = TempArray[1][1]
                    TempArray[1][1] = 0
                                                    }//else if statement

                else if (Button6.icon == EmptyImage){
                    Button6.icon = Button5.icon
                    Button5.icon = EmptyImage
                    TempArray[1][2] = TempArray[1][1]
                    TempArray[1][1] = 0
                                                    }//else if statement

                else if (Button8.icon == EmptyImage){
                    Button8.icon = Button5.icon
                    Button5.icon = EmptyImage
                    TempArray[2][1] = TempArray[1][1]
                    TempArray[1][1] = 0
                                                    }//else if statement

                                   }//else if statement

                                        }//else if statement

        else if (e!!.source == Button6){

            if (TempArray[1][2] == 0){
                when (Counter) {
                    1 -> Button6.icon = Image1
                    2 -> Button6.icon = Image2
                    3 -> Button6.icon = Image3
                    4 -> Button6.icon = Image4
                    5 -> Button6.icon = Image5
                    6 -> Button6.icon = Image6
                    7 -> Button6.icon = Image7
                    8 -> Button6.icon = Image8
                                }//when

                if (Counter == 9){
                    TempArray[1][2] = 0
                    Counter++
                                 }//if statement
                else {
                    TempArray[1][2] = Counter
                    Counter++
                     }//else statement
                                      }//if statement

            else if (Counter == 10){

                if (Button5.icon == EmptyImage){
                    Button5.icon = Button6.icon
                    Button6.icon = EmptyImage
                    TempArray[1][1] = TempArray[1][2]
                    TempArray[1][2] = 0
                                               }//if statement

                else if (Button3.icon == EmptyImage){
                    Button3.icon = Button6.icon
                    Button6.icon = EmptyImage
                    TempArray[0][2] = TempArray[1][2]
                    TempArray[1][2] = 0
                                                    }//else if statement

                else if (Button9.icon == EmptyImage){
                    Button9.icon = Button6.icon
                    Button6.icon = EmptyImage
                    TempArray[2][2] = TempArray[1][2]
                    TempArray[1][2] = 0
                                                    }//else if statement

                                   }//else if statement


                                       }//else if statement

        else if (e!!.source == Button7){

            if (TempArray[2][0] == 0){
                when (Counter) {
                    1 -> Button7.icon = Image1
                    2 -> Button7.icon = Image2
                    3 -> Button7.icon = Image3
                    4 -> Button7.icon = Image4
                    5 -> Button7.icon = Image5
                    6 -> Button7.icon = Image6
                    7 -> Button7.icon = Image7
                    8 -> Button7.icon = Image8
                                }//when

                if (Counter == 9){
                    TempArray[2][0] = 0
                    Counter++
                                 }//if statement
                else {
                    TempArray[2][0] = Counter
                    Counter++
                     }//else statement
                                     }//if statement

            else if (Counter == 10){

                if (Button4.icon == EmptyImage){
                    Button4.icon = Button7.icon
                    Button7.icon = EmptyImage
                    TempArray[1][0] = TempArray[2][0]
                    TempArray[2][0] = 0
                                               }//if statement

                else if (Button8.icon == EmptyImage){
                    Button8.icon = Button7.icon
                    Button7.icon = EmptyImage
                    TempArray[2][1] = TempArray[2][0]
                    TempArray[2][0] = 0
                                                    }//else if statement


                                    }//else if statement

                                         }//else if statement

        else if (e!!.source == Button8){

            if (TempArray[2][1] == 0){
                when (Counter) {
                    1 -> Button8.icon = Image1
                    2 -> Button8.icon = Image2
                    3 -> Button8.icon = Image3
                    4 -> Button8.icon = Image4
                    5 -> Button8.icon = Image5
                    6 -> Button8.icon = Image6
                    7 -> Button8.icon = Image7
                    8 -> Button8.icon = Image8
                                }//when

                if (Counter == 9){
                    TempArray[2][1] = 0
                    Counter++
                                 }//if statement
                else {
                    TempArray[2][1] = Counter
                    Counter++
                     }//else statement
                                      }//if statement


            else if (Counter == 10){

                if (Button5.icon == EmptyImage){
                    Button5.icon = Button8.icon
                    Button8.icon = EmptyImage
                    TempArray[1][1] = TempArray[2][1]
                    TempArray[2][1] = 0
                                               }//if statement

                else if (Button7.icon == EmptyImage){
                    Button7.icon = Button8.icon
                    Button8.icon = EmptyImage
                    TempArray[2][0] = TempArray[2][1]
                    TempArray[2][1] = 0
                                                    }//else if statement

                else if (Button9.icon == EmptyImage){
                    Button9.icon = Button8.icon
                    Button8.icon = EmptyImage
                    TempArray[2][2] = TempArray[2][1]
                    TempArray[2][1] = 0
                                                    }//else if statement

                                     }//else if statement



        }//else if statement

        else if (e!!.source == Button9){

            if (TempArray[2][2] == 0){
                when (Counter) {
                    1 -> Button9.icon = Image1
                    2 -> Button9.icon = Image2
                    3 -> Button9.icon = Image3
                    4 -> Button9.icon = Image4
                    5 -> Button9.icon = Image5
                    6 -> Button9.icon = Image6
                    7 -> Button9.icon = Image7
                    8 -> Button9.icon = Image8
                               }//when

                if (Counter == 9){
                    TempArray[2][2] = 0
                    Counter++
                                 }//if statement
                else {
                    TempArray[2][2] = Counter
                    Counter++
                     }//else statement
                                     }//if statement

            else if (Counter == 10){

                if (Button6.icon == EmptyImage){
                    Button6.icon = Button9.icon
                    Button9.icon = EmptyImage
                    TempArray[1][2] = TempArray[2][2]
                    TempArray[2][2] = 0
                                               }//if statement

                else if (Button8.icon == EmptyImage){
                    Button8.icon = Button9.icon
                    Button9.icon = EmptyImage
                    TempArray[2][1] = TempArray[2][2]
                    TempArray[2][2] = 0
                                                    }//else if statement

                                     }//else if statement

                                       }//else if statement

        else if (e!!.source == ResetButton){

            Counter = 1
            Button1.icon = EmptyImage
            Button2.icon = EmptyImage
            Button3.icon = EmptyImage
            Button4.icon = EmptyImage
            Button5.icon = EmptyImage
            Button6.icon = EmptyImage
            Button7.icon = EmptyImage
            Button8.icon = EmptyImage
            Button9.icon = EmptyImage

            for (i in 0..2)
                for (j in 0..2)
                    TempArray[i][j] = 0
                                           }//else if statement

        else if (e!!.source == StartButton) {

            var Open = PriorityQueue()
            var Closed = PriorityQueue()

            var InitialState = Node(TempArray)
            InitialState.ComputeHeuristic(TempArray, GoalState)
            Open.Insert(InitialState)

            var N: Node? = null

            while (!Open.IsEmpty() && Check == 0) {

                N = Open.GetHead()

                if (Closed.Search(N!!.GetState()) == 0)
                    Closed.Insert(N)

                    if (IsEqual(N, GoalState) == 1) {
                        End(N)
                        break
                                                    }//if statement

                    Expand(N, Open, Closed)

                                        }//while loop


            if (StackNode.empty())
                JOptionPane.showMessageDialog(this, "This 8-Puzzle can not be solved !", "Failed !", JOptionPane.INFORMATION_MESSAGE)

            else {

                var Child = 0
                JOptionPane.showMessageDialog(this,"This 8-Puzzle has been solved !","succeed ",JOptionPane.INFORMATION_MESSAGE)
                while (!StackNode.empty()) {

                    N = StackNode.peek()

                    if (N!!.GetParent() != null){

                        for (i in 0..2){
                            for (j in 0..2){

                                Child = N.GetState()[i][j]

                                    if (i == 0 && j == 0){

                                        when (Child){
                                            1-> Button1.icon = Image1
                                            2-> Button1.icon = Image2
                                            3-> Button1.icon = Image3
                                            4-> Button1.icon = Image4
                                            5-> Button1.icon = Image5
                                            6-> Button1.icon = Image6
                                            7-> Button1.icon = Image7
                                            8-> Button1.icon = Image8
                                            0-> Button1.icon = EmptyImage
                                                    }

                                        //Thread.sleep(500)
                                         
                                                         }

                                    else if (i == 0 && j == 1){

                                        when (Child){
                                            1-> Button2.icon = Image1
                                            2-> Button2.icon = Image2
                                            3-> Button2.icon = Image3
                                            4-> Button2.icon = Image4
                                            5-> Button2.icon = Image5
                                            6-> Button2.icon = Image6
                                            7-> Button2.icon = Image7
                                            8-> Button2.icon = Image8
                                            0-> Button2.icon = EmptyImage
                                                    }

                                        //Thread.sleep(500)

                                                              }

                                    else if (i == 0 && j == 2){


                                        when (Child){
                                            1-> Button3.icon = Image1
                                            2-> Button3.icon = Image2
                                            3-> Button3.icon = Image3
                                            4-> Button3.icon = Image4
                                            5-> Button3.icon = Image5
                                            6-> Button3.icon = Image6
                                            7-> Button3.icon = Image7
                                            8-> Button3.icon = Image8
                                            0-> Button3.icon = EmptyImage
                                                    }

                                        //Thread.sleep(500)

                                                               }

                                    else if (i == 1 && j == 0){


                                        when (Child){
                                            1-> Button4.icon = Image1
                                            2-> Button4.icon = Image2
                                            3-> Button4.icon = Image3
                                            4-> Button4.icon = Image4
                                            5-> Button4.icon = Image5
                                            6-> Button4.icon = Image6
                                            7-> Button4.icon = Image7
                                            8-> Button4.icon = Image8
                                            0-> Button4.icon = EmptyImage
                                                    }

                                        //Thread.sleep(500)

                                                              }

                                    else if (i == 1 && j == 1){

                                        when (Child){
                                            1-> Button5.icon = Image1
                                            2-> Button5.icon = Image2
                                            3-> Button5.icon = Image3
                                            4-> Button5.icon = Image4
                                            5-> Button5.icon = Image5
                                            6-> Button5.icon = Image6
                                            7-> Button5.icon = Image7
                                            8-> Button5.icon = Image8
                                            0-> Button5.icon = EmptyImage
                                                    }

                                        //Thread.sleep(500)

                                                              }

                                    else if (i == 1 && j == 2){

                                        when (Child){
                                            1-> Button6.icon = Image1
                                            2-> Button6.icon = Image2
                                            3-> Button6.icon = Image3
                                            4-> Button6.icon = Image4
                                            5-> Button6.icon = Image5
                                            6-> Button6.icon = Image6
                                            7-> Button6.icon = Image7
                                            8-> Button6.icon = Image8
                                            0-> Button6.icon = EmptyImage
                                                    }


                                        //Thread.sleep(500)

                                                              }

                                    else if (i == 2 && j == 0){

                                        when (Child){
                                            1-> Button7.icon = Image1
                                            2-> Button7.icon = Image2
                                            3-> Button7.icon = Image3
                                            4-> Button7.icon = Image4
                                            5-> Button7.icon = Image5
                                            6-> Button7.icon = Image6
                                            7-> Button7.icon = Image7
                                            8-> Button7.icon = Image8
                                            0-> Button7.icon = EmptyImage
                                                    }

                                        Thread.sleep(1500)

                                                              }

                                    else if (i == 2 && j == 1){

                                        when (Child){
                                            1-> Button8.icon = Image1
                                            2-> Button8.icon = Image2
                                            3-> Button8.icon = Image3
                                            4-> Button8.icon = Image4
                                            5-> Button8.icon = Image5
                                            6-> Button8.icon = Image6
                                            7-> Button8.icon = Image7
                                            8-> Button8.icon = Image8
                                            0-> Button8.icon = EmptyImage
                                                    }

                                        //Thread.sleep(500)


                                                              }

                                    else if (i == 2 && j == 2){

                                        when (Child){
                                            1-> Button9.icon = Image1
                                            2-> Button9.icon = Image2
                                            3-> Button9.icon = Image3
                                            4-> Button9.icon = Image4
                                            5-> Button9.icon = Image5
                                            6-> Button9.icon = Image6
                                            7-> Button9.icon = Image7
                                            8-> Button9.icon = Image8
                                            0-> Button9.icon = EmptyImage
                                                    }

                                        //Thread.sleep(500)

                                                              }

                                                 }//for loop
                                                    }//for loop

                                               }//if statement

                    StackNode.pop()
                    this.size = Dimension(this.size.width,this.size.height-1)
                                            }//while loop

                     }//else statement

                                             }//else if statement

                                                  }//actionPerformed


    private var Check = 0
    private var StackNode = Stack<Node>()
    private var Counter = 1
    private var TempArray : Array<IntArray> = arrayOf(intArrayOf(0,0,0), intArrayOf(0,0,0), intArrayOf(0,0,0))
    private var GoalState : Array<IntArray> = arrayOf(intArrayOf(1,2,3), intArrayOf(4,5,6), intArrayOf(7,8,0))
    private var MainPanel = JPanel()
    private var SecondPanel = JPanel()
    private var Label = JLabel("8-Puzzle Slider")
    private var EmptyImage = ImageIcon("D://Kotlin//8-Puzzle Slider//src//Empty.png")
    private var Image1  = ImageIcon("D://Kotlin//8-Puzzle Slider//src//1.png")
    private var Image2  = ImageIcon("D://Kotlin//8-Puzzle Slider//src//2.png")
    private var Image3  = ImageIcon("D://Kotlin//8-Puzzle Slider//src//3.png")
    private var Image4  = ImageIcon("D://Kotlin//8-Puzzle Slider//src//4.png")
    private var Image5  = ImageIcon("D://Kotlin//8-Puzzle Slider//src//5.png")
    private var Image6  = ImageIcon("D://Kotlin//8-Puzzle Slider//src//6.png")
    private var Image7  = ImageIcon("D://Kotlin//8-Puzzle Slider//src//7.png")
    private var Image8  = ImageIcon("D://Kotlin//8-Puzzle Slider//src//8.png")
    private var ExitImage = ImageIcon("D://Kotlin//8-Puzzle Slider//src//Exit.png")
    private var ResetImage = ImageIcon("D://Kotlin//8-Puzzle Slider//src//Reset.png")
    private var StartImage = ImageIcon("D://Kotlin//8-Puzzle Slider//src//Start.png")
    private var Button1 = JButton(EmptyImage)
    private var Button2 = JButton(EmptyImage)
    private var Button3 = JButton(EmptyImage)
    private var Button4 = JButton(EmptyImage)
    private var Button5 = JButton(EmptyImage)
    private var Button6 = JButton(EmptyImage)
    private var Button7 = JButton(EmptyImage)
    private var Button8 = JButton(EmptyImage)
    private var Button9 = JButton(EmptyImage)
    var StartButton = JButton(StartImage)
    private var ResetButton = JButton(ResetImage)
    private var ExitButton  = JButton(ExitImage)


    constructor() : super("8-Puzzle Slider"){

        setSize(600,650)
        setLocation(100,100)

        Label.background = Color.GRAY
        Label.horizontalAlignment = (Component.CENTER_ALIGNMENT).toInt()
        Label.foreground = Color.RED
        Label.font = Font("Showcard Gothic",1,35)


        var GL = GridLayout()
        GL.columns = 4
        GL.rows = 3
        GL.hgap = 2
        GL.vgap = 2
        SecondPanel.layout = GL
        SecondPanel.background = Color.WHITE


        SecondPanel.add(Button1)
        SecondPanel.add(Button2)
        SecondPanel.add(Button3)
        SecondPanel.add(StartButton)
        SecondPanel.add(Button4)
        SecondPanel.add(Button5)
        SecondPanel.add(Button6)
        SecondPanel.add(ResetButton)
        SecondPanel.add(Button7)
        SecondPanel.add(Button8)
        SecondPanel.add(Button9)
        SecondPanel.add(ExitButton)

        MainPanel.layout = BorderLayout()
        MainPanel.border = BorderFactory.createLoweredBevelBorder()
        MainPanel.add(Label,BorderLayout.NORTH)
        MainPanel.add(SecondPanel,BorderLayout.CENTER)

        add(MainPanel)
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName())
        SwingUtilities.updateComponentTreeUI(this)

        ExitButton.addActionListener(this)
        Button1.addActionListener(this)
        Button2.addActionListener(this)
        Button3.addActionListener(this)
        Button4.addActionListener(this)
        Button5.addActionListener(this)
        Button6.addActionListener(this)
        Button7.addActionListener(this)
        Button8.addActionListener(this)
        Button9.addActionListener(this)
        ResetButton.addActionListener(this)
        StartButton.addActionListener(this)


        isVisible = true
        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE

                           }//constructor

    class Node{

        private var State :Array<IntArray> = arrayOf(intArrayOf(0,0,0), intArrayOf(0,0,0), intArrayOf(0,0,0))
        private var Heuristic :Double = 0.0
        private var Parent : Node? = null
        private var Next : Node? = null

        constructor(Temp : Array<IntArray>){
            SetState(Temp)
                                           }//constructor

        fun GetState () : Array<IntArray>{
            return State
                                         }//GetState

        fun SetState (Temp: Array<IntArray>){
            for (i in 0..2)
                for (j in 0..2)
                    State[i][j] = Temp[i][j]

                                            }//SetState

        fun GetHeuristic() : Double {
            return Heuristic
                                    }//GetHeuristic

        fun SetHeuristic(Temp:Double){
            Heuristic = Temp
                                     }//SetHeuristic

        fun GetParent() : Node? {
            return Parent
                                }//GetParent

        fun SetParent(Temp : Node?) {
            Parent = Temp
                                    }//SetParent

        fun GetNext () : Node? {
            return Next
                               }//GetNext

        fun SetNext (Temp : Node?) {
            Next = Temp
                                   }//SetNext

        fun ComputeHeuristic (State : Array<IntArray>,Goal:Array<IntArray>){

            var X : Int = 0
            var Y  : Int = 0
            var X1 : Double = 0.0
            var Y1 : Double = 0.0
            for (i in 0..2){
                for (j in 0..2){
                    if (State[i][j] != 0){
                        X = (State[i][j] - 1)/3
                        X1 = Math.floor(X.toDouble())
                        X = X1.toInt()

                        Y = (State[i][j] - 1) % 3
                        Y1 = Math.floor(Y.toDouble())
                        Y = Y1.toInt()

                        X = Math.abs(X-i)
                        Y = Math.abs(Y-j)
                        Heuristic += X+Y
                                         }//if statement
                                     }//second for loop
                                 }//first for loop

                                                                            }//ComputeHeuristic

              }//Node


    class PriorityQueue{

        private var Root : Node? = null

        constructor(){
            Root = null
                     }//constructor

        fun SetHead(Temp : Node?){
            Root = Temp
                                 }//SetHead

        fun GetHead () : Node?{
            return Root
                              }//GetHead

        fun Remove (){

           if (Root == null)
               return

            Root = Root!!.GetNext()

                     }//Remove

        fun Insert(Temp : Node?){
            var Start : Node? = Root

            if (IsEmpty() || Start!!.GetHeuristic() > Temp!!.GetHeuristic()){
                Temp!!.SetNext(Root)
                Root = Temp
                                                                            }//if statement

            else {
                while (Start!!.GetNext() != null && Start.GetNext()!!.GetHeuristic() <= Temp.GetHeuristic())
                    Start = Start.GetNext();

                Temp.SetNext(Start.GetNext());
                Start.SetNext(Temp);
                 }//else
                                 }//insert

        fun IsEmpty() : Boolean {
            return Root == null
                                }//IsEmpty

        fun Search (Temp : Array<IntArray>) : Int {

            var Start : Node? = Root
            if (Start == null)
                return 0

            while (Start != null){
                if (IsEqual(Start,Temp) == 1)
                    return 1
                Start = Start.GetNext()
                                 }//while
            return 0
                                                  }//Search

                        }//PriorityQueue


    fun Expand (Temp : Node? , Open : PriorityQueue , Closed : PriorityQueue){

        var Down : Node? = null
        var Right : Node? = null
        var Up : Node? = null
        var Left : Node? = null

        if (Temp!!.GetState()[0][0] == 1 && Temp!!.GetState() [0][1] == 2 && Temp!!.GetState()[0][2] == 3 && (Temp!!.GetState()[1][0] == 0
                        || Temp!!.GetState()[1][1] == 0 || Temp!!.GetState()[1][2] == 0)){

            Down = Move (Temp,"Down")
            Right = Move(Temp,"Right")
            Left = Move(Temp,"Left")

            Open.Remove()

            if (Down != null){
                if (IsEqual(Down,GoalState) == 1) {
                    End(Down)
                    return
                                                  }//if statement

                if (Closed.Search(Down.GetState()) == 0 && Open.Search(Down.GetState()) == 0)
                    Open.Insert(Down)
                                }//if statement

            if (Right != null){
                if (IsEqual(Right,GoalState) == 1) {
                    End(Right)
                    return
                                                   }//if statement

                if (Closed.Search(Right.GetState()) == 0 && Open.Search(Right.GetState()) == 0)
                    Open.Insert(Right)
                                }//if statement


            if (Left!=null){
                if (IsEqual(Left,GoalState) == 1) {
                    End(Left)
                    return
                                                  }//if statement

                if (Closed.Search(Left.GetState()) == 0 && Open.Search(Left.GetState()) == 0)
                    Open.Insert(Left)
                           }//if statement

                                                                                            }//if statement


        else if (Temp.GetState() [0][0] == 1 && Temp.GetState()[1][0] == 4 && Temp.GetState()[2][0] == 7 && (Temp.GetState()[0][1] == 0
                        || Temp.GetState()[1][1] == 0 || Temp.GetState()[2][1] == 0)) {

            Down = Move(Temp, "Down");

            Right = Move(Temp, "Right");

            Up = Move(Temp, "Up");


            Open.Remove();


            if (Down != null){
                if (IsEqual(Down,GoalState) == 1) {
                    End(Down)
                    return
                                                  }//if statement

                if (Closed.Search(Down.GetState()) == 0 && Open.Search(Down.GetState()) == 0)
                    Open.Insert(Down)
                            }//if statement

            if (Right != null){
                if (IsEqual(Right,GoalState) == 1) {
                    End(Right)
                    return
                                                   }//if statement

                if (Closed.Search(Right.GetState()) == 0 && Open.Search(Right.GetState()) == 0)
                    Open.Insert(Right)
                             }//if statement


            if (Up != null) {
                if (IsEqual(Up,GoalState) == 1) {
                    End(Up)
                    return
                                                }//if statement

                if (Closed.Search(Up.GetState()) == 0 && Open.Search(Up.GetState()) == 0)
                    Open.Insert(Up)

                            }//if statement

                                                                                      }//else if statement
        else {

            Down = Move(Temp, "Down");

            Right = Move(Temp, "Right");

            Up = Move(Temp, "Up");

            Left = Move(Temp, "Left");

            Open.Remove();


            if (Down != null){
                if (IsEqual(Down,GoalState) == 1) {
                    End(Down)
                    return
                                                  }//if statement

                if (Closed.Search(Down.GetState()) == 0 && Open.Search(Down.GetState()) == 0)
                    Open.Insert(Down)
                             }//if statement

            if (Right != null){
                if (IsEqual(Right,GoalState) == 1) {
                    End(Right)
                    return
                                                   }//if statement

                if (Closed.Search(Right.GetState()) == 0 && Open.Search(Right.GetState()) == 0)
                    Open.Insert(Right)
                              }//if statement

            if (Up != null) {
                if (IsEqual(Up,GoalState) == 1) {
                    End(Up)
                    return
                                                }//if statement

                if (Closed.Search(Up.GetState()) == 0 && Open.Search(Up.GetState()) == 0)
                    Open.Insert(Up)

                            }//if statement

            if (Left!=null){
                if (IsEqual(Left,GoalState) == 1) {
                    End(Left)
                    return
                                                  }//if statement

                if (Closed.Search(Left.GetState()) == 0 && Open.Search(Left.GetState()) == 0)
                    Open.Insert(Left)
                            }//if statement

                }//else statement


                                                                                   }//Expand

    fun Move(Temp : Node? , Operation : String) : Node? {

        var XPosition : Int = -1
        var YPosition : Int = -1

       loop@ for (i in 0..2)
                 for (j in 0..2)
                    if (Temp!!.GetState()[i][j] == 0) {
                        XPosition = i;
                        YPosition = j;
                        break@loop
                                                    }//if statement

        var TempArray : Array<IntArray> = arrayOf(intArrayOf(0,0,0), intArrayOf(0,0,0), intArrayOf(0,0,0))

        for (i in 0..2)
            for (j in 0..2)
                TempArray[i][j] = Temp!!.GetState()[i][j]


        if (Operation.equals("Left")) {
            if (YPosition == 0)
                return null

            else {

                TempArray[XPosition][YPosition] = TempArray[XPosition][YPosition-1]
                TempArray[XPosition][YPosition-1] = 0

                 }//else statement

                                      }//first if statement

        else if (Operation == "Right") {
            if (YPosition == 2)
                return null

            else {

                TempArray[XPosition][YPosition] = TempArray[XPosition][YPosition+1];
                TempArray[XPosition][YPosition+1] = 0

               }//else statement
                                          }//else if statement

        else if (Operation == "Up") {
            if (XPosition == 0)
                return null

            else {

                TempArray[XPosition][YPosition] = TempArray[XPosition-1][YPosition];
                TempArray[XPosition-1][YPosition] = 0

                 }//else statement
                                    }//else if statement

        else if (Operation == "Down") {
            if (XPosition == 2)
                return null

            else {

                TempArray[XPosition][YPosition] = TempArray[XPosition+1][YPosition];
                TempArray[XPosition+1][YPosition] = 0

                 }//else statement
                                      }//else if statement

        var temp = Node(TempArray)
        temp.SetParent(Temp)
        temp.ComputeHeuristic(TempArray,GoalState)

        return temp
                                                      }//Move

    fun End(Temp:Node?){

        var temp : Node? = Temp

        while (temp != null){

            StackNode.push(temp)
            temp = temp.GetParent()
                            }//while loop
            Check = 1
                       }//End

                                      }//Slider

fun IsEqual(Temp : Slider.Node?, State: Array<IntArray>) : Int{

    for (i in 0..2)
        for (j in 0..2)
            if (Temp!!.GetState()[i][j] != State[i][j])
                return 0
    return 1
                                                              }//IsEqual

fun main (args : Array<String>){
    Slider()
                               }//main



